package Recruitment.task.Service;


import com.opencsv.CSVReader;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
@PropertySource("application.properties")
public class FileReader1 {
    private static final char DEFAULT_SEPARATOR = ',';
    private static final char DEFAULT_QUOTE = '"';

//public static String fileReader(String strFile) throws IOException {
//    strFile = "src/main/resources/zestawienie.csv";
//    CSVReader reader = new CSVReader(new FileReader(strFile));
//    String[] nextLine;
//    int lineNumber = 0;
//    while ((nextLine = reader.readNext()) != null) {
//        lineNumber++;
//        System.out.println("Line # " + lineNumber);
//
//        // nextLine[] is an array of values from the line
//        System.out.println(nextLine[4] + "etc...");
//    }
//}

//        private String csvFile = "/Users/mkyong/csv/country.csv";
//        private BufferedReader br = null;
//        private String line = "";
//        private String cvsSplitBy = ",";

//        try{
//
//        try {
//            br = new BufferedReader(new FileReader(csvFile));
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//        }
//        while ((line = br.readLine()) != null) {
//                String[] country = line.split(cvsSplitBy);
//                System.out.println("code= " + country[4] + " , name=" + country[5]);
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (br != null) {
//                try {
//                    br.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//
//}


    public static String fileR(String csvFile) throws FileNotFoundException {

        csvFile = "zestawienie.csv";

        Scanner scanner = new Scanner(new File(csvFile));
        while (scanner.hasNext()) {
            List<String> line = parseLine(scanner.nextLine());
            System.out.println("id= " + line.get(0) + ", code= " + line.get(1) + " , name=" + line.get(2));
        }
        scanner.close();
        return fileR(csvFile);
    }

    public static List<String> parseLine(String cvsLine) {
        return parseLine(cvsLine, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
    }

    public static List<String> parseLine(String cvsLine, char separators) {
        return parseLine(cvsLine, separators, DEFAULT_QUOTE);
    }

    public static List<String> parseLine(String cvsLine, char separators, char customQuote) {

        List<String> result = new ArrayList<>();


        if (cvsLine == null && cvsLine.isEmpty()) {
            return result;
        }

        if (customQuote == ' ') {
            customQuote = DEFAULT_QUOTE;
        }

        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuffer curVal = new StringBuffer();
        boolean inQuotes = false;
        boolean startCollectChar = false;
        boolean doubleQuotesInColumn = false;

        char[] chars = cvsLine.toCharArray();
        for (char ch : chars) {
            if (inQuotes) {
                startCollectChar = true;
                if (ch == customQuote) {
                    inQuotes = false;
                    doubleQuotesInColumn = false;
                } else {
                    if (ch == '\"') {
                        if (!doubleQuotesInColumn) {
                            curVal.append(ch);
                            doubleQuotesInColumn = true;
                        }
                    } else {
                        curVal.append(ch);
                    }
                }
            } else {
                if (ch == customQuote) {
                    inQuotes = true;

                    if (chars[0] != '"' && customQuote == '\"') {
                        curVal.append('"');
                    }
                    if (startCollectChar) {
                        curVal.append('"');
                    }
                } else if (ch == separators) {
                    result.add(curVal.toString());
                    curVal = new StringBuffer();
                    startCollectChar = false;
                } else if (ch == '\r') {
                    continue;
                } else if (ch == '\n') {
                    break;
                } else {
                    curVal.append(ch);
                }
            }

        }
        result.add(curVal.toString());
        return result;
    }
}




