package Recruitment.task.Service;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

@Component
public class FileInterface {
    private static Scanner scanner;

    static {
        try {
            scanner = new Scanner(new File("${zestawienie.csv}"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
