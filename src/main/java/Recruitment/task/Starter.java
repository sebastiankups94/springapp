package Recruitment.task;

import Recruitment.task.Service.FileReader1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class Starter implements CommandLineRunner {

    @Autowired
    FileReader1 fileReader1;

    @Override
    public void run(String... args) throws Exception {

    }
}

